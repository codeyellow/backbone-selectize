define(function (require) {
    'use strict';
    var _ = require('underscore');

    require('selectize');

    var defaultOptions = {
        ui: null,
        collection: null,
        // Destroys the selectize instance when 'before:destroy' is called.
        destroyWith: null,
        // Attribute of model that appears as text.
        text: 'name',
        // Attribute of model that is used as a value.
        value: 'id',
        // Value that will be selected after the collection has fetched.
        defaultValue: null,
        // Fetch the collection before adding options.
        fetch: true,
        // Fetch the collection while typing.
        search: false,
        // Parameter name used for searching in the collection.
        searchParameter: 'query',
        // Options given to the fetch upon search.
        searchOptions: {},
        // Wait x ms between searches.
        debounceWait: 300,
        // Classname to add to selectize while the collection is fetching.
        busyClass: null,
        // Selectize target to add the busyClass to.
        busyClassTarget: '$wrapper',
        // Format items with this function. Especially handy if you have a nested model.
        formatOption: function (model, options) {
            return {
                value: model.get(options.value),
                text: model.get(options.text)
            };
        },
    };

    return function (options) {
        options = _.extend({}, defaultOptions, options);

        var selectizeOptions = _.extend({}, options, {
            openOnFocus: true
        });

        var addModelAsOption = function (model, addAsSelected) {
            selectizeInstance.addOption(options.formatOption(model, options));

            if (addAsSelected === true || model.id === options.defaultValue) {
                selectizeInstance.addItem(model.get(options.value));
            }
        };

        var removeModelAsOption = function (model) {
            var option = model.get(options.value);
            var item = selectizeInstance.getItem(option);
            if (!item.length) {
                selectizeInstance.removeOption(option);
            }
        };

        var onCollectionReset = function (collection) {
            selectizeInstance.clearOptions();

            if (collection) {
                collection.each(addModelAsOption);
            }
        };

        var onCollectionSync = function () {
            selectizeInstance[options.busyClassTarget].removeClass(options.busyClass);
        };

        var onCollectionRequest = function () {
            selectizeInstance[options.busyClassTarget].addClass(options.busyClass);
        };

        var removeListeners = function () {
            if (options.destroyWith) {
                options.destroyWith.stopListening(options.collection, 'add', addModelAsOption);
                options.destroyWith.stopListening(options.collection, 'remove', removeModelAsOption);
                options.destroyWith.stopListening(options.collection, 'reset', onCollectionReset);
                options.destroyWith.stopListening(options.collection, 'sync', onCollectionSync);
                options.destroyWith.stopListening(options.collection, 'request', onCollectionRequest);
            }
        };

        var fillDefaults = function () {
            // Assume that the value is a Backbone model or collection if it is an object.
            if (typeof options.defaultValue !== 'object') {
                return selectizeInstance.setValue(options.defaultValue);
            }

            // A collection is added as default value.
            if (options.defaultValue.hasOwnProperty('models')) {
                // Set default values if collection is ready immediately.
                if (options.defaultValue.length) {
                    return options.defaultValue.each(function (model) {
                        addModelAsOption(model, true);
                    });
                }

                // Sync defaultValue collection with the selected item list.
                options.destroyWith.listenTo(options.defaultValue, 'add', function (model) {
                    addModelAsOption(model, true);
                });
                options.destroyWith.listenTo(options.defaultValue, 'remove', function (model) {
                    selectizeInstance.removeItem(model.get(options.value));
                });
                return options.destroyWith.listenTo(options.defaultValue, 'reset', function (collection) {
                    selectizeInstance.clear();
                    if (collection) {
                        collection.each(function (model) {
                            addModelAsOption(model, true);
                        });
                    }
                });
            }

            // Assume that model is ready to insert in list if it has an id.
            if (options.defaultValue.id) {
                return addModelAsOption(options.defaultValue, true);
            }

            // If the model is not ready, wait for a change to happen and then insert it as default.
            options.destroyWith.listenTo(options.defaultValue, 'change:id', function (model, val) {
                if (val) {
                    options.destroyWith.stopListening(options.defaultValue, 'change:id');
                    addModelAsOption(options.defaultValue, true);
                }
            });
        };

        // Fill selectize with the collection and optionally set a default model.
        var fillOptions = function () {
            options.collection.each(addModelAsOption);

            // If the collection is fetched before selectize is rendered, remove the busyClass.
            if (options.collection.length) {
                selectizeInstance[options.busyClassTarget].removeClass(options.busyClass)
            }

            if (options.defaultValue !== null) {
                fillDefaults();
            }

            // Add listeners to sync the collection.
            if (options.destroyWith) {
                removeListeners();
                options.destroyWith.listenTo(options.collection, 'add', addModelAsOption);
                options.destroyWith.listenTo(options.collection, 'remove', removeModelAsOption);
                options.destroyWith.listenTo(options.collection, 'reset', onCollectionReset);
                options.destroyWith.listenTo(options.collection, 'sync', onCollectionSync);
                options.destroyWith.listenTo(options.collection, 'request', onCollectionRequest);
            }
        };

        var fetchCollection = function (callback) {
            return options.collection.fetch(_.defaults({
                success: callback
            }, options.searchOptions));
        };

        var selectizeSearch = function (query, callback) {
            options.collection.attributes.unset(options.searchParameter);
            if (!query.length) {
                return callback([]);
            }

            options.collection.attributes.set(options.searchParameter, query);

            fetchCollection(function () {
                var list = [];
                options.collection.each(function (model) {
                    list.push(options.formatOption(model, options));
                });

                callback(list);
            });
        };

        if (options.search === true) {
            // Fetch the collection with a search parameter when typing,
            // with a 300ms delay to prevent unnecessary requests to the server.
            selectizeOptions.load = _.debounce(function (query, callback) {
                selectizeSearch(query, callback);
            }, options.debounceWait);
        }

        if (!(options.ui instanceof jQuery)) {
            console.warn('ui parameter is not a jQuery object!');
            return;
        }

        options.ui.selectize(selectizeOptions);

        var selectizeInstance = options.ui[0].selectize;

        if (options.fetch === true && options.collection) {
            // Fetch the whole collection.
            fetchCollection(fillOptions);
        }

        if (options.fetch === false && options.collection) {
            fillOptions();
        }

        // Destroy selectize when the marionette before:destroy event is triggered.
        if (options.destroyWith !== null) {
            options.destroyWith.on('before:destroy', function () {
                selectizeInstance.destroy();
            });
        }

        // Clean up any attached listeners.
        selectizeInstance.on('destroy', removeListeners);

        return selectizeInstance;
    };
});
