# backbone-selectize

This package adds some useful shortcuts when using [Selectize.js](https://github.com/brianreavis/selectize.js) with [Backbone.js](http://backbonejs.org/) and [Marionette](http://marionettejs.com/).

Features:

- Fetch given collection and adds each model as an option.
- Or, fetch collection while typing (with a `query` parameter).
- Destroy Selectize when view is destroyed.
- Specify a default value.
- Adds Sass styles (Selectize.js uses LESS by default).

## Usage

Import Sass styles:

```scss
@import 'scss/selectize';
```

In a `Marionette.View`:

```js
var selectize = require('backbone-selectize'),
    CCustomer = require('collection/customer');

Marionette.ItemView.extend({
    onRender: function () {
        selectize({
            ui: this.ui.customer,
            collection: new CCustomer(),
            placeholder: 'Filter customer',
            destroyWith: this
        });
    }
});
```

`selectize()` returns a Selectize instance.

| __Option__ | __Description__ | __Type__ | __Default__ |
| ---        | ---             | ---      | ---         |
| `ui` | jQuery element that selectize is applied on. | `jQuery` | `null` |
| `collection` | Backbone collection that is fed to selectize. | `Backbone.Collection` | `null` |
| `destroyWith` | Unbind selectize when the given option emits a `before:destroy` event. | `Marionette.View` | `null` |
| `text` | Attribute of model that is used as label and search text. | `string` | `'name'` |
| `id` | Attribute of model that is used as a value. | `string` | `'id'` |
| `defaultValue` | Value that should be selected after fetching the collection. | `mixed` | `null` |
| `fetch` | When `true`, fetches the collection and adds the options after fetching. When `false`, immediately adds the collection as options. | `boolean` | `true` |
| `search` | When `true`, fetches the collection while typing. | `boolean` | `false` |
| `searchParameter` | Parameter name used for searching in the collection. Only used if `search` is `true`. | `string` | `'query'` |
| `searchOptions` | Options passed to collection.fetch. Only used if `search` is `true`. | `object` | `{}` |
| `debounceWait` | How many `ms` to wait between searches. Only used if `search` is `true`. | `integer` | `300` |
| `busyClass` | Classname to add to selectize while the collection is fetching. | `string` | `''` |
| `busyClassTarget` | Selectize target to add the busyClass to. | `string` | `'$wrapper'` |

You can also use all [Selectize options](https://github.com/brianreavis/selectize.js/blob/master/docs/usage.md).

## Todo

- Refresh options on collection reset / add / remove.
